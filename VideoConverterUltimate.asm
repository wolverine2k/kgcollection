; Tested on Xilisoft Video Converter Ultimate 7.8.16

.code
Gen_VideoConverterUltimate proc out_serial: DWORD
    invoke RtlZeroMemory, offset VidConU_TmpStr, sizeof VidConU_TmpStr

    xor ecx, ecx
    mov esi, offset VidConU_Alpha
    mov edi, out_serial
    .while ecx < 19
        invoke random, 16
        movzx eax, byte ptr ds:[esi+eax]
        mov byte ptr ds:[edi+ecx], al
        inc ecx
    .endw
    mov byte ptr ds:[edi+4], "-"
    mov byte ptr ds:[edi+9], "-"
    mov byte ptr ds:[edi+14], "-"
    mov byte ptr ds:[edi+19], "-"
    mov ecx, 20
    mov esi, out_serial
    mov edi, offset VidConU_Magic+65
    rep movsb
    invoke MD5Init
    invoke MD5Update, offset VidConU_Magic, 116
    invoke MD5Final
    xor ecx, ecx
    mov edi, offset VidConU_TmpStr
    .while ecx < MD5_DIGESTSIZE
        movzx ebx, byte ptr ds:[eax+ecx]
        and ebx, 0F0h
        movzx edx, byte ptr ds:[eax+ecx+1]
        shr edx, 4
        or ebx, edx
        mov byte ptr ds:[edi], bl
        add ecx, 2
        inc edi
    .endw
    mov ax, word ptr ds:[VidConU_TmpStr]
    bswap eax
    shr eax, 16
    mov bx, word ptr ds:[VidConU_TmpStr+2]
    bswap ebx
    shr ebx, 16
    mov cx, word ptr ds:[VidConU_TmpStr+4]
    bswap ecx
    shr ecx, 16
    mov dx, word ptr ds:[VidConU_TmpStr+6]
    bswap edx
    shr edx, 16
    mov edi, out_serial
    add edi, 20
    invoke wsprintf, edi, offset VidConU_SerForm, eax, ebx, ecx, edx
    Ret
Gen_VideoConverterUltimate endp
