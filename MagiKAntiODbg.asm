.code
Gen_MagiKAntiODbg proc in_username: DWORD, in_computerid: DWORD, out_serial: DWORD
    mov eax, in_username
    movzx eax, byte ptr ds:[eax]
    invoke wsprintf, offset MagAODbg_TempSt, offset MagAODbg_wsform, eax
    push eax
    invoke StrLen, in_username
    add eax, in_username
    dec eax
    movzx eax, byte ptr ds:[eax]
    pop ecx
    add ecx, offset MagAODbg_TempSt
    invoke wsprintf, ecx, offset MagAODbg_wsform, eax
    add eax, offset MagAODbg_TempSt + 1
    movzx ecx, byte ptr ds:[eax]
    add cl, byte ptr ds:[MagAODbg_TempSt]
    sub ecx, 60h
    push ecx

    invoke StrLen, in_computerid
    mov ecx, eax
    mov ebx, in_computerid
    xor edx, edx
    .while ecx != 00h
        dec ecx
        movzx eax, byte ptr [ebx+ecx]
        sub eax, 30h
        add edx, eax
    .endw
    add edx, edx

    pop eax
    imul eax, edx
    add eax, 21h
    push eax

    ; Yes, I'm actually bruting this part because I'm lazy
    .repeat
        invoke RtlZeroMemory, offset MagAODbg_TempSt, sizeof MagAODbg_TempSt
        mov ebx, 07h
        xor ecx, ecx
        mov edi, offset MagAODbg_TempSt
        .repeat
            invoke random, 10
            add eax, 30h
            mov byte ptr [edi+ecx], al
            inc ecx
            add eax, ebx
            add eax, ebx
            mov ebx, eax
        .until ebx >= 0F08h
    .until ebx == 0F08h

    invoke atol, offset MagAODbg_TempSt
    pop ecx
    sub eax, ecx
    invoke wsprintf, out_serial, offset MagAODbg_wsform, eax
    Ret
Gen_MagiKAntiODbg endp
