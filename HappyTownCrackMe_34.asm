.code
Gen_HappyTownCrackMe_34 proc in_username: DWORD, in_group: DWORD, out_serial: DWORD
    local ptMD4Hash: DWORD
    local ptSHA256Hash: DWORD
    local ptHavalHash: DWORD
    local ptTigerHash: DWORD
    local ptRMD160Hash: DWORD
    local ptMD5Hash: DWORD

    invoke RtlZeroMemory, offset HTCM34_TempStr, sizeof HTCM34_HexM
    invoke RtlZeroMemory, offset HTCM34_TempStr, sizeof HTCM34_TempStr
    invoke MD4Init
    invoke StrLen, in_username
    invoke MD4Update, in_username, eax
    invoke MD4Final
    mov ptMD4Hash, eax
    invoke SHA256Init
    invoke StrLen, ptMD4Hash
    invoke SHA256Update, ptMD4Hash, eax
    mov eax, ptMD4Hash
    movzx eax, byte ptr ds:[eax+0Fh]
    .if eax == 00h
        invoke StrLen, offset HTCM34_Magic0
        invoke SHA256Update, offset HTCM34_Magic0, eax
    .else
        invoke StrLen, offset HTCM34_Magic1
        invoke SHA256Update, offset HTCM34_Magic1, eax
    .endif
    invoke SHA256Final
    mov ptSHA256Hash, eax
    invoke HavalInit, 256, 3
    invoke StrLen, ptSHA256Hash
    invoke HavalUpdate, ptSHA256Hash, eax
    invoke StrLen, in_group
    invoke HavalUpdate, in_group, eax
    invoke StrLen, offset HTCM34_Magic0
    invoke HavalUpdate, offset HTCM34_Magic0, eax
    invoke HavalFinal, 1
    mov ptHavalHash, eax
    mov esi, ptHavalHash
    mov edi, offset HTCM34_TigerH
    mov ecx, TIGER_DIGESTSIZE
    rep movsb
    mov eax, ptHavalHash
    movzx eax, byte ptr ds:[eax]
    or eax, 1
    .while eax != 0
        push eax
        invoke TigerInit
        invoke TigerUpdate, offset HTCM34_TigerH, TIGER_DIGESTSIZE
        invoke TigerFinal
        mov ptTigerHash, eax
        mov esi, ptTigerHash
        mov edi, offset HTCM34_TigerH
        mov ecx, TIGER_DIGESTSIZE
        rep movsb
        pop eax
        dec eax
    .endw
    invoke RMD160Init
    invoke RMD160Update, offset HTCM34_TigerH, TIGER_DIGESTSIZE
    invoke RMD160Final
    mov ptRMD160Hash, eax
    mov byte ptr ds:[eax+7], 07Ah
    invoke MD5Init
    invoke MD5Update, ptRMD160Hash, MD5_DIGESTSIZE
    invoke MD5Final
    mov ptMD5Hash, eax
    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigD, eax
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigC, eax
    invoke bnCreate
    mov BigM, eax
    invoke bnFromHex, offset HTCM34_D, BigD
    invoke bnFromHex, offset HTCM34_N, BigN
    invoke bnFromBytesEx, ptMD5Hash, MD5_DIGESTSIZE, BigC, FALSE
    invoke bnModExp, BigC, BigD, BigN, BigM ;Operazione: M=C^D mod N
    invoke bnToHex, BigM, offset HTCM34_HexM
    invoke bnDestroy, BigM
    invoke bnDestroy, BigC
    invoke bnDestroy, BigN
    invoke bnDestroy, BigD
    invoke bnFinish
    invoke szCatStr, offset HTCM34_TempStr, in_group
    invoke szCatStr, offset HTCM34_TempStr, offset HTCM34_HexM
    invoke StrLen, offset HTCM34_TempStr
    invoke Base64Encode, offset HTCM34_TempStr, eax, out_serial
    Ret
Gen_HappyTownCrackMe_34 endp
