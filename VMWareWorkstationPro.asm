; Tested on VMware Workstation Pro 12.1.1

; 4 days it took. This is the biggest software (in terms of LOC)
; I ever worked on. Lots of calls, of DLLs and stuff. Luckily though,
; the right functions actually have nice export names, which means it's
; possible to locate them quite easily.
; So, how does it work?
; Well, basically you've got a message and its checksum as a serial.
; The message is built out of infos such as "trial or not", days
; to expiration, stuff like that. The infos are memorized in groups of
; 5bits (instead of the traditional 8) so the most time was spent on
; understanding what the actual hell was going on with all the code
; (which, btw, I think has been compiled with interleaving in mind
; based on the way operations that could be assembled one after another
; are instead scattered around).
; The checksum of the message is built calculating the SHA1 of the message
; itself, of an hash that's been in turn calculated from various infos
; pertaining to the license data (a copyright notice, license edition,
; product version, product ID, stuff like that) and of a constant block
; of 0x20 bytes. VMWare has got plenty of places to go to make current keygens
; stop working. Even a version number update would work. Interesting stuff.
; Anyway, you've got the SHA1 hash. The checksum is the 5-bytes-long
; value calculated by VMW_SHA1Sum. Then the final message is built
; by appending the message to the checksum. From there the bits of the
; 16-bytes-long "string" built this way are shuffled around.
; We expand that goddamned 5 bits of the shuffled result to 8 bits,
; so we can work with normal bytes. Guess what? That bytes are the indexes
; to use to pick a letter from the accepted alphabet (VMW_Alpha) and build a serial.
; We're almost done: these letters are shuffled around once more following
; the order dictated by VMW_LetOrder. Then we insert the hyphens and it's done.

VMW_SHA1Sum proto SHA1Addr: DWORD, Dest: DWORD
VMW_SerialShuffle proto Source: DWORD, Dest: DWORD

.code
Gen_VMWareWorkstationPro proc out_serial: DWORD
    invoke RtlZeroMemory, offset VMW_RandomMsg, sizeof VMW_RandomMsg
    invoke RtlZeroMemory, offset VMW_MsgChecksum, sizeof VMW_MsgChecksum
    invoke RtlZeroMemory, offset VMW_Serial5Bit, sizeof VMW_Serial5Bit
    invoke RtlZeroMemory, offset VMW_SerialIndxs, sizeof VMW_SerialIndxs
    invoke RtlZeroMemory, offset VMW_SrScattered, sizeof VMW_SrScattered
    invoke RtlZeroMemory, offset VMW_TempString, sizeof VMW_TempString

    xor ecx, ecx
    mov edi, offset VMW_RandomMsg
    .while ecx < 0Ah
        invoke random, 100h
        mov byte ptr [edi+ecx], al
        inc ecx
    .endw
    invoke random, 20h
    mov byte ptr [edi+ecx], al

    ; se vogliamo evitare i controlli per la trial questa cinquina deve dare
    ; un risutato compreso fra 1 e 5 (inclusi). Per non perdere tutti gli altri
    ; 3 bit del byte pero' prima scegliamo il numero casuale tra 1 e 5, poi...
    invoke random, 05h
    inc eax
    shl eax, 1
    push eax
    ; ...scegliamo un altro byte, questa volta usando l'intero intervallo
    ; che abbiamo a disposizione, azzeriamo i bit dove ci interessa inserire la
    ; nostra cinquina anti-trial e... inseriamo la cinquina anti-trial.
    ; Best observed using Windoze Calc's binary notation.
    invoke random, 100h
    pop ebx
    and al, 0C1h
    or al, bl
    mov byte ptr [edi+08], al

    invoke SHA1Init
    invoke SHA1Update, offset VMW_RandomMsg, 0Bh
    invoke StrLen, offset VMW_SHAConstStr
    invoke SHA1Update, offset VMW_SHAConstStr, eax
    invoke SHA1Update, offset VMW_SHAConstBlk, 20h
    invoke SHA1Final

    invoke VMW_SHA1Sum, eax, offset VMW_MsgChecksum

    mov ecx, 5
    mov esi, offset VMW_MsgChecksum
    mov edi, offset VMW_TempString
    rep movsb
    mov ecx, 0Bh
    mov esi, offset VMW_RandomMsg
    mov edi, offset VMW_TempString + 5
    rep movsb

    invoke VMW_SerialShuffle, offset VMW_TempString, offset VMW_Serial5Bit

    mov esi, offset VMW_Serial5Bit
    mov edi, offset VMW_SerialIndxs
    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx
    .while edx < 13
        mov eax, dword ptr ds:[esi+edx]
        shr eax, cl
        and eax, 1Fh
        mov byte ptr ds:[edi+ebx], al
        inc ebx
        mov eax, dword ptr ds:[esi+edx]
        add ecx, 5
        shr eax, cl
        and eax, 1Fh
        mov byte ptr ds:[edi+ebx], al
        inc edx
        inc ebx
        sub ecx, 3
    .endw

    mov ebx, offset VMW_Alpha
    mov esi, offset VMW_SerialIndxs
    mov edi, offset VMW_SrScattered
    xor ecx, ecx
    .while ecx < 25
        movzx eax, byte ptr ds:[esi+ecx]
        movzx eax, byte ptr ds:[ebx+eax]
        mov byte ptr ds:[edi+ecx], al
        inc ecx
    .endw

    mov ebx, offset VMW_LetOrder
    mov esi, offset VMW_SrScattered
    mov edi, out_serial
    xor ecx, ecx
    .while ecx < 25
        movzx eax, byte ptr ds:[esi+ecx]
        movzx edx, byte ptr ds:[ebx+ecx]
        mov byte ptr ds:[edi+edx], al
        inc ecx
    .endw
    mov byte ptr ds:[edi+5], 2Dh
    mov byte ptr ds:[edi+11], 2Dh
    mov byte ptr ds:[edi+17], 2Dh
    mov byte ptr ds:[edi+23], 2Dh
    Ret
Gen_VMWareWorkstationPro endp

VMW_SHA1Sum proc SHA1Addr: DWORD, Dest: DWORD
    mov eax, SHA1Addr
    mov bl, byte ptr ds:[eax+19]
    xor bl, byte ptr ds:[eax+14]
    xor bl, byte ptr ds:[eax+09]
    xor bl, byte ptr ds:[eax+04]
    shl ebx, 8
    mov bl, byte ptr ds:[eax+18]
    xor bl, byte ptr ds:[eax+13]
    xor bl, byte ptr ds:[eax+08]
    xor bl, byte ptr ds:[eax+03]
    shl ebx, 8
    mov bl, byte ptr ds:[eax+17]
    xor bl, byte ptr ds:[eax+12]
    xor bl, byte ptr ds:[eax+07]
    xor bl, byte ptr ds:[eax+02]
    shl ebx, 8
    mov bl, byte ptr ds:[eax+16]
    xor bl, byte ptr ds:[eax+11]
    xor bl, byte ptr ds:[eax+06]
    xor bl, byte ptr ds:[eax+01]
    xor edx, edx
    shld edx, ebx, 8
    shl ebx, 8
    mov bl, byte ptr ds:[eax+15]
    xor bl, byte ptr ds:[eax+10]
    xor bl, byte ptr ds:[eax+05]
    xor bl, byte ptr ds:[eax+00]
    mov eax, Dest
    mov dword ptr ds:[eax], ebx
    mov byte ptr ds:[eax+4], dl
    Ret
VMW_SHA1Sum endp

VMW_SerialShuffle proc uses ebx esi Source: DWORD, Dest: DWORD
    local SecondHalfTotal: DWORD
    local FirstHalfTotal: DWORD
    local SecondHalfCounter: DWORD
    local FirstHalfCounter: DWORD
    local SecondHalfBitmask: DWORD
    local FirstHalfBitmask: DWORD
    local BitTotalCounter: DWORD

    mov edi, Dest
    xor eax, eax
    mov SecondHalfTotal, 50h
    mov FirstHalfTotal, 28h
    mov SecondHalfCounter, eax
    .repeat
        mov ecx, SecondHalfTotal
        xor ebx, ebx
        mov edx, 1
        rol edx, cl
        mov ecx, FirstHalfTotal
        mov esi, 1
        mov BitTotalCounter, eax
        mov FirstHalfCounter, edx
        mov edx, 1
        rol edx, cl
        mov ecx, SecondHalfCounter
        rol esi, cl
        mov SecondHalfBitmask, edx
        mov FirstHalfBitmask, esi
        .repeat
            mov esi, FirstHalfBitmask
            mov eax, 0AAAAAAABh
            mul ebx
            shr edx, 1
            lea eax, dword ptr ds:[edx+edx*2]
            mov ecx, ebx
            sub ecx, eax
            .if ecx != 00h && ebx != 14h
                test bl, 01
                jnz @f
                .if ebx != 10h
                    mov ecx, SecondHalfTotal
                    mov eax, Source
                    mov edx, ecx
                    shr edx, 5
                    mov eax, dword ptr ds:[eax+edx*4]
                    mov edx, FirstHalfCounter
                    and eax, edx
                    neg eax
                    sbb eax, eax
                    inc ecx
                    neg eax
                    rol edx, 1
                    mov SecondHalfTotal, ecx
                    mov FirstHalfCounter, edx
                .else
                @@:
                    mov ecx, FirstHalfTotal
                    mov eax, Source
                    mov edx, ecx
                    shr edx, 5
                    mov eax, dword ptr ds:[eax+edx*4]
                    mov edx, SecondHalfBitmask
                    and eax, edx
                    neg eax
                    sbb eax, eax
                    inc ecx
                    neg eax
                    rol edx, 1
                    mov FirstHalfTotal, ecx
                    mov SecondHalfBitmask, edx
                .endif
            .else
                mov ecx, SecondHalfCounter
                mov eax, Source
                mov edx, ecx
                shr edx, 5
                mov eax, dword ptr ds:[eax+edx*4]
                and eax, esi
                neg eax
                sbb eax, eax
                inc ecx
                neg eax
                rol esi, 1
                mov SecondHalfCounter, ecx
                mov FirstHalfBitmask, esi
            .endif
            mov ecx, BitTotalCounter
            .if eax != 00h
                mov edx, ecx
                shr edx, 5
                lea eax, dword ptr ds:[edi+edx*4]
                and ecx, 1fh
                mov edx, 1
                shl edx, cl
                or dword ptr ds:[eax], edx
            .else
                mov eax, ecx
                and ecx, 1fh
                mov edx, 1
                shl edx, cl
                shr eax, 5
                lea eax, dword ptr ds:[edi+eax*4]
                not edx
                and dword ptr ds:[eax], edx
            .endif
            inc BitTotalCounter
            mov eax, BitTotalCounter
            inc ebx
        .until ebx >= 15h
    .until eax >= 69h

    mov eax, 200h
    mov edx, 6Bh
    mov BitTotalCounter, 4
    .repeat
        mov esi, Source
        lea ecx, dword ptr ds:[edx-2]
        shr ecx, 5
        lea ecx, dword ptr ds:[ecx*4]
        test dword ptr ds:[ecx+esi], eax
        je short @AndPrimo
        or dword ptr ds:[ecx+edi], eax
        jmp short @Secondo

        @AndPrimo:
        mov ebx, eax
        not ebx
        and dword ptr ds:[ecx+edi], ebx

        @Secondo:
        lea ecx, dword ptr ds:[edx-1]
        shr ecx, 5
        rol eax, 1
        lea ecx, dword ptr ds:[ecx*4]
        test dword ptr ds:[ecx+esi], eax
        je short @AndSecondo
        or dword ptr ds:[ecx+edi], eax
        jmp short @Terzo

        @AndSecondo:
        mov ebx, eax
        not ebx
        and dword ptr ds:[ecx+edi], ebx

        @Terzo:
        mov ecx, edx
        shr ecx, 5
        rol eax, 1
        lea ecx, dword ptr ds:[ecx*4]
        test dword ptr ds:[ecx+esi], eax
        je short @AndTerzo
        or dword ptr ds:[ecx+edi], eax
        jmp short @Quarto

        @AndTerzo:
        mov ebx, eax
        not ebx
        and dword ptr ds:[ecx+edi], ebx

        @Quarto:
        lea ecx, dword ptr ds:[edx+1]
        shr ecx, 5
        rol eax, 1
        lea ecx, dword ptr ds:[ecx*4]
        test dword ptr ds:[ecx+esi], eax
        je short @AndQuarto
        or dword ptr ds:[ecx+edi], eax
        jmp short @Quinto

        @AndQuarto:
        mov ebx, eax
        not ebx
        and dword ptr ds:[ecx+edi], ebx

        @Quinto:
        lea ecx, dword ptr ds:[edx+2]
        shr ecx, 5
        rol eax, 1
        lea ecx, dword ptr ds:[ecx*4]
        test dword ptr ds:[ecx+esi], eax
        je short @AndQuinto
        or dword ptr ds:[ecx+edi], eax
        jmp short @GoOn

        @AndQuinto:
        mov esi, eax
        not esi
        and dword ptr ds:[ecx+edi], esi

        @GoOn:
        rol eax, 1
        add edx, 5
        sub BitTotalCounter, 1
    .until BitTotalCounter == 0
    Ret
VMW_SerialShuffle endp
