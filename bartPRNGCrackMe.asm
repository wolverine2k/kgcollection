PRNGCM_GenKey proto BrutedSeed: DWORD, UserName: DWORD, Serial: DWORD
PRNGCM_SeedBrute proto MagicKey: DWORD

.code
Gen_bartPRNGCrackMe proc in_username: DWORD, in_magickey: DWORD, out_serial: DWORD
    invoke PRNGCM_SeedBrute, in_magickey
    .if eax <= 0FFh
        invoke PRNGCM_GenKey, eax, in_username, out_serial
    .else
        mov esi, offset PRNGCM_NoPRNG
        mov edi, out_serial
        mov ecx, sizeof PRNGCM_NoPRNG
        rep movsb
    .endif
    Ret
Gen_bartPRNGCrackMe endp

PRNGCM_GenKey proc  BrutedSeed: DWORD, username: DWORD, serial: DWORD
    local PairOffset: DWORD

    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigD, eax
    invoke bnCreate
    mov BigC, eax
    invoke bnCreate
    mov BigM, eax

    mov eax, BrutedSeed
    imul eax, eax, 80h
    add eax, offset NDTable
    mov PairOffset, eax

    mov eax, PairOffset
    invoke bnFromBytesEx, eax, 40h, BigN, FALSE
    mov eax, PairOffset
    add eax, 40h
    invoke bnFromBytesEx, eax, 40h, BigD, FALSE

    ;Generiamo finalmente il bignum che diventera' il seriale
    invoke szRev, username, username
    invoke StrLen, username
    invoke bnFromBytes, username, eax, BigC, FALSE
    invoke bnModExp, BigC, BigD, BigN, BigM

    invoke bnToHex, BigM, serial

    invoke bnDestroy, BigM
    invoke bnDestroy, BigC
    invoke bnDestroy, BigD
    invoke bnDestroy, BigN
    invoke bnFinish
    Ret
PRNGCM_GenKey endp

PRNGCM_SeedBrute proc   MagicKey: DWORD
    local Counter: DWORD

    invoke bnInit, 0FA0h

    invoke bnCreate
    mov BigTmp, eax

    invoke bnFromHex, MagicKey, BigTmp
    invoke bnToBytesEx, BigTmp, offset PRNGCM_TmString
    invoke bnDestroy, BigTmp
    invoke bnFinish

    mov Counter, 0

    .while Counter <= 0FFh
        mov ecx, 40h
        mov edi, offset PRNGCM_TmString
        mov esi, Counter
        imul esi, esi, 80h
        add esi, offset NDTable
        repz cmpsb
        je @f
        inc Counter
    .endw

    @@:
    mov eax, Counter
    Ret
PRNGCM_SeedBrute endp