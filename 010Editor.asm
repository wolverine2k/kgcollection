; Tested on SweetScape's 010 Editor 6.0.3
OlOEditor3S   proto rndris: DWORD, crc: DWORD
OlOEditorCRCS proto in_username: DWORD, namelen: DWORD, rndris: DWORD

.code
Gen_010Editor proc in_username: DWORD, out_serial: DWORD
    local namelen: DWORD
    local usersnumber: DWORD
    local crc: DWORD
    local primo: DWORD
    local secondo: DWORD
    local terzo: DWORD
    local quarto: DWORD

    invoke szUpper, in_username
    invoke StrLen, in_username
    mov namelen, eax
    mov usersnumber, 03E8h  ; 03E8h -> Site license
    invoke OlOEditorCRCS, in_username, namelen, usersnumber
    mov crc, eax

    invoke OlOEditor3S, usersnumber, crc
    movzx ebx, ax
    mov primo, ebx
    shr eax, 16
    add eax, 9Ch
    mov secondo, eax
    mov eax, crc
    bswap eax
    shr eax, 16
    mov terzo, eax
    mov eax, crc
    bswap eax
    and eax, 0FFFFh
    mov quarto, eax
    invoke wsprintf, out_serial, offset OlO_SerialFormat, primo, secondo, terzo, quarto
    Ret
Gen_010Editor endp

OlOEditor3S proc usersnumber: DWORD, crc: DWORD
    mov eax, usersnumber
    mov ebx, crc

    ;uno e due
    imul eax, 0Bh
    xor eax, 3421h
    sub eax, 4D30h
    xor eax, 7892h
    and eax, 0FFFFh
    mov ecx, eax

    ;zero               ; keyversion
    invoke random, 0F9h ; FFh - 06h = F9h
    mov edx, eax
    add edx, 06h        ; we make sure we pick a version number between 6 and 255
    xor edx, 0A7h
    sub edx, 3Dh
    xor edx, 18h
    and edx, 0FFh

    mov eax, ecx
    shl eax, 8
    add eax, edx
    xchg ah, al
    shr ebx, 8
    xor eax, ebx

    ror eax, 8
    xchg ah, al
    Ret
OlOEditor3S endp

OlOEditorCRCS proc in_username: DWORD, namelen: DWORD, usersnumber: DWORD
    local t1

    xor ecx, ecx
    xor edi, edi
    xor esi, esi
    mov t1, 00h

    mov eax, usersnumber
    mov ebx, eax
    shl ebx, 4
    sub ebx, eax
    .while ecx < namelen
        push ecx
        mov eax, in_username
        movzx eax, byte ptr ds:[eax+ecx]
        mov ecx, dword ptr ds:[eax*4+OlOEditTbl]
        lea edx, dword ptr ds:[eax+0Dh]
        and edx, 0FFh
        add ecx, edi
        xor ecx, dword ptr ds:[edx*4+OlOEditTbl]
        add eax, 2Fh
        and eax, 0FFh
        imul ecx, dword ptr ds:[eax*4+OlOEditTbl]
        mov edx, esi
        and edx, 0FFh
        add ecx, dword ptr ds:[edx*4+OlOEditTbl]
        mov edx, t1
        mov eax, ebx
        and eax, 0FFh
        add ecx, dword ptr ds:[eax*4+OlOEditTbl]
        and edx, 0FFh
        add ecx, dword ptr ds:[edx*4+OlOEditTbl]
        add t1, 13h
        add esi, 9h
        add ebx, 0Dh
        mov edi, ecx
        pop ecx
        inc ecx
    .endw
    mov eax, edi
    Ret
OlOEditorCRCS endp
