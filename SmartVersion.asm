; Tested on Gilles Vollant Software SmartVersion 3.60

.code
Gen_SmartVersion proc in_username: DWORD, out_serial: DWORD
    local namelen: DWORD
    local multiplier: DWORD
    local namenum: DWORD

    mov namenum, 0047694Ch
    invoke szUpper, in_username
    invoke StrLen, in_username
    mov namelen, eax
    mov multiplier, eax
    xor ecx, ecx
    .while ecx <= namelen
        mov eax, ecx
        mov ebx, 14
        cdq
        div ebx
        .if edx == 00h
            mov multiplier, 27h
        .endif
        mov eax, multiplier
        mov ebx, in_username
        movzx ebx, byte ptr ds:[ebx+ecx]
        mul ebx
        add eax, namenum
        mov namenum, eax
        lea eax, dword ptr ds:[ecx+3]
        mov ebx, 14
        cdq
        div ebx
        mov eax, multiplier
        .if edx != 00h
            mov ebx, 3
        .else
            mov ebx, 7
        .endif
        mul ebx
        mov multiplier, eax
        inc ecx
    .endw

    invoke random, 4
    mov eax, dword ptr ds:[SmartVer_MgcVal+eax*4]
    add eax, namenum
    invoke wsprintf, offset SmartVer_TmpStr, offset SmartVer_SerFor, eax

    xor ecx, ecx
    .while ecx < 8
        .if byte ptr ds:[SmartVer_TmpStr+ecx] == 38h
            mov byte ptr ds:[SmartVer_TmpStr+ecx], 42h
        .elseif byte ptr ds:[SmartVer_TmpStr+ecx] == 42h
            mov byte ptr ds:[SmartVer_TmpStr+ecx], 38h
        .endif
        inc ecx
    .endw

    mov ecx, 2
    mov esi, offset SmartVer_TmpStr
    mov edi, out_serial
    rep movsd
    Ret
Gen_SmartVersion endp
