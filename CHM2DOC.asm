; Tested on SoftAny CHM to DOC converter 3.07

.code
Gen_CHM2DOC proc out_serial: DWORD
    @@:
    ; Generate a random serial. This will be the base we'll
    ; adjust to match the wanted criteria
    ; We want the sum of every byte of the serial multiplied for its
    ; position inside the serial to be equal to 2863h
    ; Letter1 * 1 + Letter2 * 2 + LetterN * N... = 2863h
    ; Exit the loop only when the sum is > 2863h
    xor ebx, ebx
    xor ecx, ecx
    mov edi, out_serial
    .repeat
        invoke random, 36
        movzx eax, byte ptr [CHM2DOC_Alpha+eax]
        mov byte ptr [edi+ecx], al
        inc ecx
        imul eax, ecx
        add ebx, eax
    .until ebx >= 2863h

    ; We're out of the loop. We are sure the serial as of now does NOT meet
    ; the wanted criteria (unless we exited because the generated serial gave the
    ; exact sum). Zero out the last letter so that we can adjust it
    ; by calculating the difference between the wanted value and the actual sum,
    ; dividing by the current index and adding the result to current letter,
    ; then decrementing the index, taking the rest of the division and dividing
    ; and adding again until the index is >= 1. We have to stop there since
    ; 1) we are now at the beginning of the generated serial
    ; 2) we do not want to divide by 0 and destroy the universe
    mov esi, ecx
    dec esi
    mov byte ptr [edi+ecx-1], 00h
    sub ebx, eax
    mov eax, 2863h
    sub eax, ebx
    .while ecx >= 1
        cdq
        idiv ecx
        add byte ptr [edi+ecx-1], al
        mov eax, edx
        dec ecx
    .endw

    ; Check that every letter in the generated serial
    ; is present in the alphabet we have chosen (CHM2DOC_Alpha)
    mov edx, esi
    xor esi, esi
    .while esi <= edx
        mov eax, out_serial
        movzx eax, byte ptr ds:[eax+esi]
        mov edi, offset CHM2DOC_Alpha
        mov ecx, sizeof CHM2DOC_Alpha
        repne scasb
        .if ecx == 0
            xor eax, eax
            mov ecx, edx
            inc ecx
            mov edi, out_serial
            rep stosb
            jmp @b
        .endif
        inc esi
    .endw
    Ret
Gen_CHM2DOC endp
