; Tested on VeryPDF HTML Converter 2.0

.code
Gen_HTMLConverter proc out_serial: DWORD
    local primo: DWORD
    local secondo: DWORD
    local diciannovesimo: DWORD
    local ventesimo: DWORD
    local sesto: DWORD
    local quattordicesimo: DWORD

    .repeat
        invoke random, 9h
        mov edx, eax
    .until dl != 0h
    mov byte ptr [primo], dl
    mov al,0Ah
    sub al,dl
    mov byte ptr [secondo],al

    .repeat
        invoke random, 9h
        mov edx, eax
    .until dl != 0h
    mov byte ptr [diciannovesimo], dl
    mov al,0Ah
    sub al,dl
    mov byte ptr [ventesimo],al

    .repeat
        invoke random, 9h
        mov edx, eax
    .until dl != 0h
    mov byte ptr [sesto], dl
    mov al,8h
    sub al,dl
    mov byte ptr [quattordicesimo],al

    add byte ptr [primo], 30h
    add byte ptr [secondo], 30h
    add byte ptr [diciannovesimo], 30h
    add byte ptr [ventesimo], 30h
    add byte ptr [sesto], 30h
    add byte ptr [quattordicesimo], 30h

    xor ebx, ebx
    mov edi, out_serial
    .while ebx < 14h
        invoke random, 9h
        mov edx, eax
        add dl, 30h
        mov byte ptr [edi+ebx], dl
        inc ebx
    .endw

    mov eax, out_serial
    mov dl, byte ptr [primo]
    mov byte ptr [eax], dl
    mov dl, byte ptr [secondo]
    mov byte ptr [eax+1h], dl
    mov dl, byte ptr [diciannovesimo]
    mov byte ptr [eax+12h], dl
    mov dl, byte ptr [ventesimo]
    mov byte ptr [eax+13h], dl
    mov dl, byte ptr [sesto]
    mov byte ptr [eax+5h], dl
    mov dl, byte ptr [quattordicesimo]
    mov byte ptr [eax+0Dh], dl
    mov byte ptr [eax+0Ch], 4Dh
    mov byte ptr [eax+0Eh], 4Ch
    Ret
Gen_HTMLConverter endp
