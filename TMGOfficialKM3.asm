.code
Gen_TMGOfficialKM3 proc in_username: DWORD, out_serial: DWORD
    invoke MD5Init
    invoke StrLen, in_username
    invoke MD5Update, in_username, eax
    invoke MD5Final
    mov ecx, 10h
    mov esi, eax
    mov edi, offset TMG3_TempString
    rep movsb

    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigTmp, eax
    invoke bnCreate
    mov BigH, eax
    invoke bnCreate
    mov BigMMod, eax
    invoke bnCreate
    mov BigM, eax
    Invoke bnCreate
    mov BigP, eax
    invoke bnCreate
    mov BigK, eax
    invoke bnCreate
    mov BigG, eax
    invoke bnCreate
    mov BigR, eax
    invoke bnCreate
    mov BigX, eax

    invoke bnFromBytesEx, offset TMG3_TempString, 10h, BigH, FALSE
    invoke bnFromHex, offset TMG3_FirstPow, BigTmp
    invoke bnFromHex, offset TMG3_MMod, BigMMod
    invoke bnModExp, BigH, BigTmp, BigMMod, BigM

    invoke bnFromHex, offset TMG3_P, BigP

    @@:
    invoke bnRandom, BigK, 40h
    invoke bnCmp, BigK, BigP
    cmp eax, -1
    jne @b
    invoke bnGCD, BigK, BigP, BigTmp
    mov eax, BigTmp
    cmp dword ptr ds:[eax], 01
    jne @b
    add eax, 08h
    cmp dword ptr ds:[eax], 01
    jne @b

    invoke bnFromHex, offset TMG3_G, BigG
    invoke bnInc, BigP
    invoke bnModExp, BigG, BigK, BigP, BigR
    invoke bnDec, BigP

    invoke bnFromHex, offset TMG3_X, BigX
    invoke bnModInv, BigK, BigP, BigK
    invoke bnAdd, BigM, BigP
    invoke bnMul, BigX, BigR, BigTmp
    invoke bnMod, BigTmp, BigP, BigTmp
    invoke bnSub, BigM, BigTmp
    invoke bnMod, BigM, BigP, BigTmp
    invoke bnMul, BigTmp, BigK, BigTmp
    invoke bnMod, BigTmp, BigP, BigTmp

    mov eax, out_serial
    invoke bnToHex, BigR, eax
    mov eax, out_serial
    add eax, 10h
    invoke bnToHex, BigTmp, eax

    invoke bnDestroy, BigTmp
    invoke bnDestroy, BigH
    invoke bnDestroy, BigMMod
    invoke bnDestroy, BigM
    invoke bnDestroy, BigP
    invoke bnDestroy, BigK
    invoke bnDestroy, BigG
    invoke bnDestroy, BigR
    invoke bnDestroy, BigX
    invoke bnFinish
    Ret
Gen_TMGOfficialKM3 endp
