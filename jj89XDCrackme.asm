.code
Gen_jj89XDCrackme proc in_username: DWORD, out_serial: DWORD
    xor eax, eax
    mov edi,offset jjCM_TempString
    mov ecx, sizeof jjCM_TempString
    rep stosb
    invoke StrLen, in_username
    invoke HexEncode, in_username,eax,offset jjCM_TempString
    mov eax, offset jjCM_TempString
    .repeat
        .if byte ptr ds:[eax] == 39h
            mov byte ptr ds:[eax], 70h
        .endif
        inc eax
    .until byte ptr ds:[eax] == 00h

    xor ebx,ebx
    xor ecx,ecx
    mov eax, offset jjCM_TempString
    mov edx, out_serial
    .repeat
        mov bl, byte ptr [eax]
        mov byte ptr [edx+ecx], bl
        .if ecx == 02h
            inc ecx
            mov byte ptr [edx+ecx], 2Dh
        .endif
        inc ecx
        inc eax
    .until byte ptr ds:[eax] == 00h
    Ret
Gen_jj89XDCrackme endp
