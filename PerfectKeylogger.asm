; Tested on BlazingTools Perfect Keylogger 1.68.2

.code
Gen_PerfectKeylogger proc in_username: DWORD, out_serial: DWORD
    local NameLen: DWORD
    local NameCounter: DWORD
    local SerialCounter: DWORD
    local MagicStringCounter: DWORD

    mov NameCounter, 0
    mov SerialCounter, 0
    mov MagicStringCounter, 0
    invoke StrLen, in_username
    mov NameLen, eax

    .while SerialCounter != 19
        mov eax, in_username
        add eax, NameCounter
        movzx ebx, byte ptr ds:[eax]
        mov eax, offset PK_MagicString
        add eax, MagicStringCounter

        xor bl, byte ptr ds:[eax]
        movzx eax, bl
        mov ecx, 19h
        cdq
        div ecx
        add edx, 041h

        mov eax, out_serial
        add eax, SerialCounter
        mov byte ptr ds:[eax], dl

        inc NameCounter
        inc SerialCounter
        inc MagicStringCounter

        mov eax, NameCounter
        .if eax == NameLen
            mov NameCounter, 0
        .endif

        .if SerialCounter == 4 || SerialCounter == 9 || SerialCounter == 14
            mov eax, out_serial
            add eax, SerialCounter
            mov byte ptr ds:[eax], 2Dh
            inc SerialCounter
        .endif
    .endw

    Ret
Gen_PerfectKeylogger endp
