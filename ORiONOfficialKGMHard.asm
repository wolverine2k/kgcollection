; Now, this KeygenMe has got a bug and we can't do anything about that:
; the username hash gets encoded in ASCII
; the bignum resulting from the powmod operation gets encoded in ASCII too
; the two strings are compared and if they match the serial is correct.
; Trouble begins when the username hash begins with 0:
; the library used to manage the bignums (the same used to encode a bignum in ASCII)
; avoids all of the 0s at the beginning of the bignum and starts printing out a string
; only when the byte to encode is != 00h.
; So, for the username SerMadafaka:
; hash in ASCII: 04886410848A220084AA601604A0425685A02256
; bignum encoded in ASCII: 4886410848A220084AA601604A0425685A02256
; The calcs executed and the bignum outputted by the library are correct,
; but the string comparison will fail because of this difference.
; I'm still wondering who the hell thought comparing two strings
; instead of two bignums was a good idea...

.code
Gen_ORiONOfficialKGMHard proc in_username: DWORD, out_serial: DWORD
    local namelen: DWORD

    invoke RtlZeroMemory, offset OROffKGMH_Hash, sizeof OROffKGMH_Hash
    invoke StrLen, in_username
    mov namelen, eax
    xor eax, eax
    xor ebx, ebx
    xor ecx, ecx
    mov ebx, namelen
    mov eax, in_username
    movzx eax, byte ptr ds:[eax+ecx]
    .repeat
        imul eax, eax, 11001100h
        rol ax, cl
        imul eax, ebx
        xor eax, 110011h
        add eax, 58392831h
        rcl eax, cl
        and eax, 5666AAA5h
        xor dword ptr ds:[OROffKGMH_Hash], eax
        imul eax, eax, 0F34526CAh
        rol ax, cl
        imul eax, ebx
        xor eax, 652324h
        add eax, 98542312h
        rcl eax, cl
        and eax, 5666AAA5h
        xor dword ptr ds:[OROffKGMH_Hash+4], eax
        imul eax, eax, 22553355h
        rol ax, cl
        imul eax, ebx
        xor eax, 22BB44h
        add eax, 23476CACh
        rcl eax, cl
        and eax, 5666AAA5h
        xor dword ptr ds:[OROffKGMH_Hash+8], eax
        imul eax, eax, 0AEBCA011h
        rol ax, cl
        imul eax, ebx
        xor eax, 0E475AFh
        add eax, 54BEA1CAh
        rcl eax, cl
        and eax, 5666AAA5h
        xor dword ptr ds:[OROffKGMH_Hash+12], eax
        imul eax, eax, 8F3A3B3Ch
        rol ax, cl
        imul eax, ebx
        xor eax, 0BA3212h
        add eax, 0CAFECAFEh
        rcl eax, cl
        and eax, 5666AAA5h
        xor dword ptr ds:[OROffKGMH_Hash+16], eax
        inc ecx
        mov eax, in_username
        movzx eax, byte ptr ds:[eax+ecx]
    .until eax == 00h

    movzx eax, byte ptr ds:[OROffKGMH_Hash]
    shr eax, 4
    .if eax == 00h
        ; It's not like we couldn't, but the serial wouldn't be accepted
        ; because the KeygenMe (hereby generically referred to as "the software") is bugged.
        ; Small customer care lesson: when your keygen generates a
        ; (technically) valid key but the software is bugged and doesn't
        ; accept it do not let the user see any key. Shift the blame, tell
        ; him its a technical limit or something and you won't be flooded by
        ; e-mails telling you your keygen sucks (when, actually, the software does).
        mov esi, offset OROffKGMH_NoJoy
        mov edi, out_serial
        mov ecx, sizeof OROffKGMH_NoJoy
        rep movsb
    .else
        invoke bnInit, 0FA0h
        invoke bnCreate
        mov BigD, eax
        invoke bnCreate
        mov BigN, eax
        invoke bnCreate
        mov BigC, eax
        invoke bnCreate
        mov BigM, eax
        invoke bnFromHex, offset OROffKGMH_D, BigD
        invoke bnFromHex, offset OROffKGMH_N, BigN
        invoke bnFromBytesEx, offset OROffKGMH_Hash, 20, BigC, FALSE
        invoke bnModExp, BigC, BigD, BigN, BigM ;Operazione: M=C^D mod N
        invoke bnToHex, BigM, out_serial
        invoke bnDestroy, BigM
        invoke bnDestroy, BigC
        invoke bnDestroy, BigN
        invoke bnDestroy, BigD
        invoke bnFinish
    .endif
    Ret
Gen_ORiONOfficialKGMHard endp
