.code
Gen_TMGOfficialKM2 proc in_username: DWORD, in_company: DWORD, out_serial: DWORD
    xor eax, eax
    mov ecx, 05h
    mov edi, offset TMG2_TempString
    rep stos dword ptr es:[edi]
    invoke StrLen, in_username
    mov ecx, eax
    mov eax, -1
    mov esi, in_username
    mov edi, offset TMG2_TempString
    xor edx, edx
    .while ecx > 0
        lods byte ptr ds:[esi]
        xor ah, al
        imul eax, eax, 89177313h
        and eax, 55AA55AAh
        imul eax, eax, 12299381h
        xor eax, 0AA55AA11h
        imul eax, eax, 61h
        xor ah, al
        or eax, 10030118h
        imul eax, eax, 988279h
        rcl eax, cl
        xor dword ptr ds:[edi+edx], eax
        add edx, 03h
        and edx, 0Fh
        inc edx
        dec ecx
    .endw

    push eax
    invoke StrLen, in_company
    mov ecx, eax
    pop eax
    mov esi, in_company
    mov edi, offset TMG2_TempString
    xor edx, edx
    .while ecx > 0
        lods byte ptr ds:[esi]
        sub ah, al
        imul eax, eax, 89157313h
        and eax, 55AA55AAh
        imul eax, eax, 12299387h
        or eax, 0AA55AA11h
        imul eax, eax, 61h
        xor eax, 10010114h
        imul eax, eax, 7918279h
        xor ah, al
        rcr eax, cl
        xor dword ptr ds:[edi+edx], eax
        add edx, 03h
        and edx, 0Fh
        inc edx
        dec ecx
    .endw
    sub eax, dword ptr ds:[edi+08h]
    imul eax, eax, 34814815h
    add dword ptr ds:[edi+10h], eax
    shr eax, 0Bh
    and eax, 03h
    mov byte ptr ds:[edi], al

    invoke bnInit, 0FA0h
    invoke bnCreate
    mov BigD, eax
    invoke bnCreate
    mov BigN, eax
    invoke bnCreate
    mov BigC, eax
    invoke bnCreate
    mov BigM, eax
    invoke bnFromHex, offset TMG2_D, BigD
    invoke bnFromHex, offset TMG2_N, BigN
    invoke bnFromBytesEx, offset TMG2_TempString, 14h, BigC, FALSE
    invoke bnModExp, BigC, BigD, BigN, BigM ;Operazione: M=C^D mod N
    invoke bnToHex, BigM, out_serial
    invoke bnDestroy, BigM
    invoke bnDestroy, BigC
    invoke bnDestroy, BigN
    invoke bnDestroy, BigD
    invoke bnFinish
    Ret
Gen_TMGOfficialKM2 endp
