W332_CustCRC proto startCRC: DWORD, lpWord: DWORD, len: DWORD

.code
Gen_Woot332KeygenMe proc in_username: DWORD, out_serial: DWORD
    invoke StrLen, in_username
    invoke W332_CustCRC, 0, in_username, eax
    mov WKGM_CRC1, eax
    xor CRC32Table+15h, 01Bh
    invoke W332_CustCRC, 0, in_username, 4
    mov WKGM_CRC2, eax
    xor CRC32Table+15h, 01Bh
    invoke HexEncode, offset WKGM_CRC1, 4, out_serial
    mov eax, out_serial
    add eax, 8
    invoke HexEncode, offset WKGM_CRC2, 4, eax
    Ret
Gen_Woot332KeygenMe endp

W332_CustCRC proc startCRC: DWORD, lpWord: DWORD, len: DWORD
    mov eax, startCRC
    xor ecx, ecx
    not eax
    .if len > 00h
        .while ecx < len
            mov edx,lpWord
            movzx edx, byte ptr ds:[ecx+edx]
            xor dl, al
            shr eax, 08h
            xor eax, dword ptr ds:[edx*4+offset CustCRCTbl+1]
            inc ecx
            inc eax
        .endw
    .endif
    not eax
    Ret
W332_CustCRC endp
